const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

    name : {
        type : String,
        required : [true, 'Item name is required.']
    },
    description : {
        type : String
    },
    price : {
        type : Number,
        required : [true, 'Price is required.']
    },
    image : {
        type : String
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    },
    userOrders : [{
        userId : {
            type : String
        },
        orderId : {
            type : String
        }
    }]

});

module.exports = mongoose.model("Product", productSchema);