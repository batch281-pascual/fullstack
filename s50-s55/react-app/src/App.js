import { Container} from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login'
import Logout from './pages/Logout'
import NoPageFound from './pages/NoPageFound'

import { useState, useEffect } from 'react';

import CourseView from './components/CourseView'

// import the UserProvider from the UserContext to provide the content or value of our UserContext

import { UserProvider } from './UserContext';

import './App.css';

// import necessary modules from react-router-dom
import { BrowserRouter, Route, Routes } from 'react-router-dom'

// React JS is SPA (Single Page Application)
// Whenever a link is clicked, it functions as if the page is being reloaded but what actually happens is it goes through the process or rendering, mounting, re-rendering and remounting.


// The 'BrowserRouter' component will enable us to simulate page navigation by synchronizing the show content and the shown URL in the web browser

// The 'Routes' component holds all our Route components. It selects which 'Route' components to show based on the URL endpoint.
// For example, when the '/courses' is visisted in the web browser React.js will show the Courses component to us

function App() {

  // State hook for the user state that's defined here for global scoping.

  // Initialized as a the value of the user information from browser's localStorage

  // This will be used to store the user info. and will be used for validating if a user is logged on the app or not

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // The unsetUser Function is for clearing localStorage on logout

  const unsetUser = () => {
    localStorage.clear()
  };

  // useEffect( () => {
  //   console.log(user)
  // }, [user])

  useEffect( () => {

      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers : {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(response => response.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })

  }, [])


  return (
     <UserProvider value = { { user, setUser, unsetUser } }>
        <BrowserRouter>
           <AppNavBar />
             <Container>
                 <Routes>
                   <Route path = '/' element = {<Home />} />
                   <Route path = '/courses' element = {<Courses />} />
                   <Route path = '/register' element = {<Register />} />
                   <Route path = '/login' element = {<Login />} />
                   <Route path = '/logout' element = {<Logout />} />
                   <Route path = '/courses/:id' element = {<CourseView />} /> 
                   <Route path = '*' element = {<NoPageFound />} />
                 </Routes>
             </Container>
        </BrowserRouter>
     </UserProvider>
  );
}

export default App;
