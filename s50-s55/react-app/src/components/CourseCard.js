import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

export default function CourseCard ({courseProp}) {

	// Checks to see if the data was successfully passed
	// console.log(courseProp);

	const { _id, name, description, price } = courseProp;

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components

	const [ count, setCount ] = useState(0)

	const [seats, setSeats] = useState(3)

	// We are going to create a new state that will declare or tell the value of the disabled property in the button.

	const [isDisabled, setIsDiabled] = useState(false);

	// Function that keeps track of the enrolees
	// The setter function for useState are asynchronous allowing it to execute seperately from other codes in the program
	// The "setCount" function is being executed while the "console.log" is already completed


	// MY ACTIVITY CODE

	// function enroll() {

	// 	if( seats > 1 ){
	// 		// alert("No more seats available!");
	// 		alert("Congratulations on getting the last slot!");
	// 		// document.querySelector('#btn-submit').setAttribute('disabled', true);
	// 	} else {
	// 		setCount( count + 1 );
	// 		setSeats( seats - 1 );
	// 	}

	// }


	// ACTIVITY CODE

	function enroll() {

		if( seats > 1 ){
			setCount( count + 1 );
			setSeats( seats - 1 );

		} else {
			alert("Congratulations on getting the last slot!");
			setSeats(seats - 1);
		}

	}


	// Define a 'useEffect' hook to have 'CourseCard' component do or perform a certain task after every changes in the seats state
	// The side effect will run automatically in initial rendering and in every changes of the seats state.
	// The array in the useEffect is called the dependency array 

	useEffect( () => {
		
		if( seats === 0){
			setIsDiabled(true);
		}


	}, [seats])
 


	return (

			<Card>
	            <Card.Body>
	                <Card.Title>{ name }</Card.Title>
	                <Card.Subtitle>Description:</Card.Subtitle>
	                <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/JavaScript-logo.png/800px-JavaScript-logo.png" alt="" height="150px" />
	                <Card.Text>{ description }</Card.Text>
	                <Card.Subtitle>Price:</Card.Subtitle>
	                <Card.Text>{ price }</Card.Text>
	                <Card.Subtitle>Enrollees: { count }</Card.Subtitle>
	                <Card.Subtitle>Seats available: { seats }</Card.Subtitle>
	                <Button as = {Link} to = {`/courses/${_id}`} variant="primary" id="btn-submit" disabled={isDisabled}>see more details</Button>
	            </Card.Body>
	        </Card>

	)
}

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next

CourseCard.propTypes = {
	
	// The "shape" method is used to check if a prop object conforms to a specific shape

	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}