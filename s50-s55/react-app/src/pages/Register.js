import { Button, Form, Row, Col } from 'react-bootstrap';
// import Login from './pages/Login';

import { Navigate, useNavigate } from 'react-router-dom';

// We need to import the useState from the react
import { useState, useEffect, useContext } from 'react';

import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Register() {

	const navigate = useNavigate();

	const { user } = useContext(UserContext);

	// State hooks to store the value of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	
	// Add Firstname, Lastname and Mobile number useState
	
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);
	const [isPasswordMatch, setIsPasswordMatch] = useState(true);

	// This useEffect will disable or enable our sign up button
	useEffect( () => {

		// We are going to add if statement and all the condition that we mention should be satisfied before we enable the signup button

		if( 
			email !== '' && 
			password1 !== '' && 
			password2 !== '' && 
			password1 === password2 && 
			email.length <= 15 && 
			firstName !== '' && 
			lastName !== '' && 
			mobileNo.length === 11 )
		{
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	}, [email, password1, password2, firstName, lastName, mobileNo])


	// Password Validation Error if password1 is unmatch in password2
	useEffect( () => {
		if(password2 === password1){
			setIsPasswordMatch(true);
		} else if (password2 !== '') {
			setIsPasswordMatch(false);
		} else if (password2.length === 0) {
			setIsPasswordMatch(true);
		} else {
			setIsPasswordMatch(false);
		}
	}, [password1, password2])


	// Function to simulate user registration
	function registerUser(e) {
		// Prevent page re-loading
		e.preventDefault();

		// alert('Thank you for Registering!');


		/* *************** ACTIVITY CODE - S55 START ***************** */


		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'			},
			body : JSON.stringify({
				email : email,
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data){
			let timerInterval
			Swal2.fire({
				title: 'Email already exist! Please try again!',
				icon: 'error',
				text: 'Check your email address and try again',
				  html: 'I will close in <b></b> milliseconds.',
				  timer: 3000,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal2.showLoading()
				    const b = Swal2.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				      b.textContent = Swal2.getTimerLeft()
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})	
			} else {

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'			},
			body : JSON.stringify({
				firstName : firstName,
				lastName : lastName,
				mobileNo : mobileNo,
				email : email,
				password : password1,
				// password2 : password1
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data === true){
				let timerInterval
				Swal2.fire({
					title: 'Login Successful!',
					icon: 'success',
					  html: 'I will close in <b></b> milliseconds.',
					  timer: 3000,
					  timerProgressBar: true,
					  didOpen: () => {
					    Swal2.showLoading()
					    const b = Swal2.getHtmlContainer().querySelector('b')
					    timerInterval = setInterval(() => {
					      b.textContent = Swal2.getTimerLeft()
					    }, 100)
					  },
					  willClose: () => {
					    clearInterval(timerInterval)
					  }
					})

			navigate('/login');

			} else{
				let timerInterval
				Swal2.fire({
					title: 'Login unsuccessful!',
					icon: 'error',
					  html: 'I will close in <b></b> milliseconds.',
					  timer: 3000,
					  timerProgressBar: true,
					  didOpen: () => {
					    Swal2.showLoading()
					    const b = Swal2.getHtmlContainer().querySelector('b')
					    timerInterval = setInterval(() => {
					      b.textContent = Swal2.getTimerLeft()
					    }, 100)
					  },
					  willClose: () => {
					    clearInterval(timerInterval)
					  }
					})
			}
		})




			}
		})

		/* *************** ACTIVITY CODE - S55 END ***************** */



		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword1('');
		setPassword2('');
	} 

	return (
		    
	  	user.id === null || user.id === undefined
	  	?
	  	  	<Row>
	  	  		<Col className="col-6 mx-auto">
	  	  			<h1 className="text-center">Register Form</h1>
	  	  			<Form onSubmit = {event => registerUser(event)}>
	  				     
	  				     {/***************************** ACTIVITY CODE - S55 START **********************************/}

		  	  				<Form.Group className="mb-3" controlId="formBasicPassword1">
		  	  				  <Form.Label>First Name</Form.Label>
		  	  				  <Form.Control
		  	  				  type="text" 
		  	  				  placeholder="Enter First Name"
		  	  				  value={firstName}
		  	  				  onChange={e => setFirstName(e.target.value)}
		  	  				  />
		  	  				</Form.Group>

		  	  				<Form.Group className="mb-3" controlId="formBasicPassword2">
		  	  				  <Form.Label>Last Name</Form.Label>
		  	  				  <Form.Control
		  	  				  type="text" 
		  	  				  placeholder="Enter Last Name"
		  	  				  value={lastName}
		  	  				  onChange={e => setLastName(e.target.value)}
		  	  				  />
		  	  				</Form.Group>

		  	  				<Form.Group className="mb-3" controlId="formBasicPassword3">
		  	  				  <Form.Label>Mobile Number</Form.Label>
		  	  				  <Form.Control
		  	  				  type="number" 
		  	  				  placeholder="Enter First Name"
		  	  				  value={mobileNo}
		  	  				  onChange={e => setMobileNo(e.target.value)}
		  	  				  />
		  	  				</Form.Group>

	  				     {/***************************** ACTIVITY CODE - S55 END **********************************/}

	  				      <Form.Group className="mb-3" controlId="formBasicEmail">
	  				        <Form.Label>Email address</Form.Label>
	  				        <Form.Control
	  				        type="email" 
	  				        placeholder="Enter email"
	  				        value={email}
	  				        onChange={e => setEmail(e.target.value)}
	  				         />
	  				      </Form.Group>

	  				      <Form.Group className="mb-3" controlId="formBasicPassword4">
	  				        <Form.Label>Password</Form.Label>
	  				        <Form.Control
	  				        type="password" 
	  				        placeholder="Password"
	  				        value={password1}
	  				        onChange={e => setPassword1(e.target.value)}
	  				        />
	  				      </Form.Group>

	  				      <Form.Group className="mb-3" controlId="formBasicPassword5">
	  				        <Form.Label>Confirm Password</Form.Label>
	  				        <Form.Control
	  				        type="password"
	  				        placeholder="Confirm your Password"
	  				        value={password2}
	  				        onChange={e => setPassword2(e.target.value)}
	  				        />
	  				        <Form.Text className="text-danger" hidden = {isPasswordMatch}>
	  				          Password does not match! Please Try Again!
	  				        </Form.Text>
	  				      </Form.Group>
	  				      
	  				      <Button 
	  				      className="w-100"
	  				      variant="primary" 
	  				      type="submit"
	  				      disabled = {isDisabled}
	  				      >Sign Up</Button>	
	  				      
	  	  			</Form>
	  	  		</Col>
	  	  	</Row>

	  	  	:

	  	  	<Navigate to = '*' />

	)
}