const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

mongoose.connect("mongodb+srv://mjordanpascual:FGRcaKmOh9QH41yO@wdc028-course-booking.sbuuomu.mongodb.net/s37-41API?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

mongoose.connection.once('open', () => console.log('Now Connected to MongoDB Atlas!'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended : true }));



app.listen(process.env.PORT || 4000, () => {
    console.log(`Backend API server is now online & connected on port ${process.env.PORT || 4000}`);
});
