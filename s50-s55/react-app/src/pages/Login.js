import { useState, useEffect, useContext } from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom'

// sweetalert2 is a simple and useful package for generating user alerts with ReactJS 
import Swal2 from 'sweetalert2'

import UserContext from '../UserContext';

export default function Login() {

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// State to determine whether submit buttin is enabled or not

	const [isDisabled, setIsDisabled] = useState(true);

	useEffect( () => {

		if(email !== '' && password !== '' && email.length <= 15){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password])


	// Allows us to consume the UserContext object and it's properties to use for user validation

	const { user, setUser }  = useContext(UserContext);


	// const [user, setUser] = useState(localStorage.getItem('email'));


	function authenticate(e){
		e.preventDefault();

		// Set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('property', value);

		// The localStorage.setItem() allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering.

		// Because REACT JS is a single page application, using the localStorage will not trigger rerendering of component.

		// localStorage.setItem('email', email);
		// localStorage.setItem('password', password);


		// setUser(localStorage.getItem('email'));

		// alert('You are Logged In!');

		// navigate('/courses');
		
		// setEmail('');
		// setPassword('');

		// Process fetch request to the corresponding backend API
		// Syntax:
			 /*fetch('url', {options}).then(response => response.json()).then(
				data => {})
			*/

			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: 'POST',
				headers: {
					'Content-Type' : 'application/json'
				},
				body: JSON.stringify({
					email : email,
					password : password
				})
			})
			.then(response => response.json())
			.then(data => {
				// console.log(data);

				// if statement to check whether the login is successful.
				if(data === false){
					// alert('Login unsuccessful!')

					// in adding sweetalert2 you have to use the fire method
					// Swal2.fire({
					// 	title: 'Login unsuccessful!',
					// 	icon: 'error',
					// 	text: 'Check your login credentials and try again'
					// })

					let timerInterval
					Swal2.fire({
						title: 'Login unsuccessful!',
						icon: 'error',
						text: 'Check your login credentials and try again',
						  html: 'I will close in <b></b> milliseconds.',
						  timer: 3000,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal2.showLoading()
						    const b = Swal2.getHtmlContainer().querySelector('b')
						    timerInterval = setInterval(() => {
						      b.textContent = Swal2.getTimerLeft()
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						})


				} else {
					localStorage.setItem('token', data.access)
					
					retrieveUserDetails(data.access)

					// alert('Login successful')

					let timerInterval
					Swal2.fire({
						title: 'Login Successful!',
						icon: 'success',
						text: 'Welcome to Zuitt!',
						  html: 'I will close in <b></b> milliseconds.',
						  timer: 3000,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal2.showLoading()
						    const b = Swal2.getHtmlContainer().querySelector('b')
						    timerInterval = setInterval(() => {
						      b.textContent = Swal2.getTimerLeft()
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						})
					
					navigate('/courses')
				}


			})

	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			headers : {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data => {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	return ( 
		  	user.id === null || user.id === undefined
		  	?
		  		  	<Row>
		  		  		<Col className="col-6 mx-auto">
		  		  			<h1 className="text-center">Login Form</h1>
		  		  			<Form onSubmit = {event => authenticate(event)}>
		  					      <Form.Group className="mb-3" controlId="formBasicEmail">
		  					        <Form.Label>Email address</Form.Label>
		  					        <Form.Control
		  					        type="email" 
		  					        placeholder="Enter email"
		  					        value={email}
		  					        onChange={e => setEmail(e.target.value)}
		  					         />
		  					      </Form.Group>

		  					      <Form.Group className="mb-3" controlId="formBasicPassword1">
		  					        <Form.Label>Password</Form.Label>
		  					        <Form.Control
		  					        type="password"
		  					        placeholder="Password"
		  					        value={password}
		  					        onChange={e => setPassword(e.target.value)}
		  					        />
		  					      </Form.Group>

		  					      <Button 
		  					      variant="outline-primary" 
		  					      type="submit"
		  					      disabled = {isDisabled}
		  					      >
		  					       Login
		  					      </Button>
		  		  			</Form>
		  		  		</Col>
		  		  	</Row>
		  	:
		  	<Navigate to = '/*' />
		)
}
